#Silenzioh Bot

##How to set dev environment
1. Install `python`, `python-pip` and `virtualenv` on your device
2. Clone git repository
3. `cd` in project directory
4. Build virtual environment with `virtualenv venv -p python3`
5. Activate virtual env with `python venv/bin/activate`
6. Install requirements with `pip install -r requirements.txt`
7. Run main file with `python main.py`

##How to contribute
[Make a pull request](https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html)

##Notes
This is the new repository of silenziohbot whithout all the commits made before 09/10/2017. Reason: hardcoded sensitive datas. Shit. 

****

Hosting by:
[![Cyberia Network](https://cyberianetwork.com/static/img/logo-small.png)](https://cyberianetwork.com)