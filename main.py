#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Made by:   Andrea Carriero
#            info@andreacarriero.com
#            https://andreacarriero.com
#
# Hosted by: Cyberia Network
#            https://cyberianetwork.com

from telegram.ext import Updater, CommandHandler, Job
from telegram.ext import MessageHandler, Filters
import logging, datetime, pytz, random
import string, os, json


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


############# CONFIGURATION #############
# Switches
SEND_DAILY_CRONTAB = True
SEND_SILENZIOH = True
# Parameters
JOB_DELAY = 60
DAYLY_CRONTAB_TIME = datetime.time(7, 30)
IDS_FILE = 'ids.txt'
MESSAGES_FILE = 'messages.json'
PING = False
TELEGRAM_TOKEN = os.environ['TELEGRAM_BOT_TOKEN']
# Subjects
class Subjects():
    programm = "Programmazione"
    mat_disc = "Matematica discreta"
    arch = "Architettura degli elaboratori e SO" 
############# END CONFIG #############

# Global var
ENGINE_STARTED = False

# Load old chat ids from local storage
with open(IDS_FILE) as f:
    chat_ids = [int(id.strip()) for id in f.readlines()]
    logging.info("Loaded previously saved chat ids from file.")
    logging.info(str(chat_ids))

# Load messages
with open(MESSAGES_FILE) as f:
    messages = json.load(f)
    logging.info("Loaded Messages")

calendar = [
    {
        'lesson': Subjects.programm,
        'day': 0,
        'time': datetime.time(8, 30)
    },
    {
        'lesson': Subjects.programm,
        'day': 0,
        'time': datetime.time(9, 30)
    },
    {
        'lesson': Subjects.programm,
        'day': 0,
        'time': datetime.time(10, 30)
    },
    {
        'lesson': Subjects.mat_disc,
        'day': 0,
        'time': datetime.time(11, 30)
    },
    {
        'lesson': Subjects.mat_disc,
        'day': 0,
        'time': datetime.time(12,30)
    },
    {
        'lesson': Subjects.mat_disc,
        'day': 0,
        'time': datetime.time(13, 30)
    },

    {
        'lesson': Subjects.mat_disc,
        'day': 1,
        'time': datetime.time(8, 30)
    },
    {
        'lesson': Subjects.mat_disc,
        'day': 1,
        'time': datetime.time(9, 30)
    },
    {
        'lesson': Subjects.arch,
        'day': 1,
        'time': datetime.time(10, 30)
    },
    {
        'lesson': Subjects.arch,
        'day': 1,
        'time': datetime.time(11, 30)
    },
    {
        'lesson': Subjects.arch,
        'day': 1,
        'time': datetime.time(12, 30)
    },

    {
        'lesson': Subjects.arch,
        'day': 2,
        'time': datetime.time(8, 30)
    },
    {
        'lesson': Subjects.arch,
        'day': 2,
        'time': datetime.time(9, 30)
    },
    {
        'lesson': Subjects.arch,
        'day': 2,
        'time': datetime.time(10, 30)
    },
    {
        'lesson': Subjects.programm,
        'day': 2,
        'time': datetime.time(11, 30)
    },
    {
        'lesson': Subjects.programm,
        'day': 2,
        'time': datetime.time(12, 30)
    },
    {
        'lesson': Subjects.programm,
        'day': 2,
        'time': datetime.time(13, 30)
    },

    {
        'lesson': Subjects.mat_disc,
        'day': 3,
        'time': datetime.time(8, 30)
    },
    {
        'lesson': Subjects.mat_disc,
        'day': 3,
        'time': datetime.time(9, 30)
    },
    {
        'lesson': Subjects.mat_disc,
        'day': 3,
        'time': datetime.time(10, 30)
    },
    {
        'lesson': Subjects.arch,
        'day': 3,
        'time': datetime.time(11, 30)
    },
    {
        'lesson': Subjects.arch,
        'day': 3,
        'time': datetime.time(12, 30)
    },

    {
        'lesson': Subjects.programm,
        'day': 4,
        'time': datetime.time(8, 30)
    },
    {
        'lesson': Subjects.programm,
        'day': 4,
        'time': datetime.time(9, 30)
    },
    {
        'lesson': Subjects.programm,
        'day': 4,
        'time': datetime.time(10, 30)
    },
    {
        'lesson': Subjects.programm,
        'day': 4,
        'time': datetime.time(11, 30)
    }
]

# Random silenzioh
def get_silenzioh():
    return random.choice(messages['silenzioh'])
# Random Fanelli
def get_fanelli():
    return random.choice(messages['fanelli'])

# Random Lanza
def get_lanza():
    return random.choice(messages['lanza'])

# Random Nardozza
def get_nardozza():
    return random.choice(messages['nardozza'])

# Start new conversation
def start(bot, update, args, job_queue, chat_data):
    global ENGINE_STARTED

    chat_id = update.message.chat_id

    # For not cached ids
    if not chat_id in chat_ids:
        chat_ids.append(chat_id)
        logging.info('New conversation. ID: %s' % (chat_id))
        logging.info('Ids list: %s' % (str(chat_ids)))

        #Save new id to file
        with open(IDS_FILE, "a") as f:
            f.write(str(chat_id) + '\n')
        logging.info("Saved %s to file" % (chat_id))

    # If the bot has just been started
    # This is executable only one time
    # Reminder: send the first "/start" to stert the engine
    if not ENGINE_STARTED:
        logging.info("Adding main job for %s" % (chat_id))
        job = job_queue.run_repeating(loop, JOB_DELAY, context=chat_id)
        chat_data['job'] = job
        ENGINE_STARTED = True

# Main job loop
def loop(bot, job):
    now = datetime.datetime.now().time().replace(second=0, microsecond=0)
    day_of_the_week = datetime.datetime.today().weekday()

    # PING
    # Only for debug
    if PING:
        for id in chat_ids:
            bot.send_message(id, "PING!")
            logger.info('Ping %s' % (id))

    #Send day crontab
    if SEND_DAILY_CRONTAB:
        if day_of_the_week != 5 and day_of_the_week != 6:
            if now == DAYLY_CRONTAB_TIME:
                logging.info("Sending day crontab to %s" % (job.context))
                day_crontab = "ORARIO DEL GIORNO %s" % (datetime.datetime.now().strftime("%d-%m-%y"))
                for event in calendar:
                    if event['day'] == day_of_the_week:
                        day_crontab = day_crontab + '\n' + event['time'].strftime("%H:%M") + " -> " + event['lesson']
                for id in chat_ids:
                    bot.send_message(id, text=day_crontab)

    #Send SILENZIOH
    if SEND_SILENZIOH:
        for event in calendar:  
            if event['lesson'] == Subjects.arch and event['day'] == day_of_the_week and event['time'] == now:
                #TODO add random delay with timedelta
                logging.info("Let's say SILENZIOH to %s" % (job.context))
                for id in chat_ids:
                    bot.send_message(id, text=get_silenzioh())

##### Command handler functions
def fanelli(bot, update, args, job_queue, chat_data):
    chat_id = update.message.chat_id
    bot.send_message(chat_id, text=get_fanelli())
    logging.info('SILENZIOH! requested in %s' % (chat_id))

def lanza(bot, update, args, job_queue, chat_data):
    chat_id = update.message.chat_id
    bot.send_message(chat_id, text=get_lanza())
    logging.info('Lanza requested in %s' % (chat_id))

def nardozza(bot, update, args, job_queue, chat_data):
    chat_id = update.message.chat_id
    bot.send_message(chat_id, text=get_nardozza())
    logging.info('Nardozza requested in %s' % (chat_id))

#### Message handler functions
def parse_text(bot, update):
    chat_id = update.message.chat_id
    text = update.message.text

    # Remove ounctuation from text
    for char in string.punctuation:
        text = text.replace(char, ' ')

    # Words cases
    if 'slide' in text:
        logging.info('Someone said ADA in %s' % (chat_id))
        bot.send_message(chat_id=chat_id, text="Per l'ultima volta... LE SLIDES SONO SUA ADAAAA HO DETTO ADAAAAA!")

#### Error handler functions
def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"' % (update, error))


def main():
    updater = Updater(TELEGRAM_TOKEN)

    # Get the dispatcher
    dp = updater.dispatcher

    # Add comman handlers
    dp.add_handler(CommandHandler("start", start, pass_args=True, pass_job_queue=True, pass_chat_data=True))
    dp.add_handler(CommandHandler("fanelli", fanelli, pass_args=True, pass_job_queue=True, pass_chat_data=True))
    dp.add_handler(CommandHandler("lanza", lanza, pass_args=True, pass_job_queue=True, pass_chat_data=True))
    dp.add_handler(CommandHandler("nardozza", nardozza, pass_args=True, pass_job_queue=True, pass_chat_data=True))

    # Add message handlers
    dp.add_handler(MessageHandler(Filters.text, parse_text))

    # Add error handlers
    # This logs all the errors
    dp.add_error_handler(error)

    # Start the Bot

    # This is non-blocking function:
    updater.start_polling()

    # This is blocking function:
    updater.idle()

if __name__ == '__main__':
    main()